﻿namespace App.ViewModel

open System
open System.Collections.ObjectModel
open System.Windows
open System.Windows.Data
open System.Windows.Input
open System.ComponentModel
open System.Text
open App.Model
open GeneticAlgorithm

type TextBox = System.Windows.Controls.TextBox

type MainWindowViewModel() as this =
    inherit ViewModelBase()

    let mutable nodeCount = 4
    let mutable chromosomeLength = 4
    let mutable populationSize = 2
    let mutable generationSparseProbability = 0.2
    let mutable crossPoints = 1
    let mutable mutationSparseProbability = 0.2
    let mutable isRunMode = false
    let mutable startNodeIndex = -1
    let mutable endNodeIndex = -1
    let mutable generationCount = 10
    let mutable stepCount = 0
    let mutable iterationCount = 0
    let mutable pathManager: Option<PathManager> = None
    let mutable geneticAlgorithm: Option<GeneticAlgorithm> = None

    let nodeNames = [|"A"; "B"; "C"; "D"; "E"; "F"; "G"; "H"; "I"; "J"; "K"; "L"; "M"; "N"; "O"; "P"|]
    let minWeight = 1
    let maxWeight = 100
    let random = Random()
    let log = StringBuilder()

    let rec factor n = 
        if n > 1 then n * factor (n - 1) else n

    let rowToRecord (node: string) (row: ResizeArray<int>) =
        { Node = node;
          A = row.[0].ToString();
          B = row.[1].ToString();
          C = row.[2].ToString();
          D = if row.Count > 3 then row.[3].ToString() else "";
          E = if row.Count > 4 then row.[4].ToString() else "";
          F = if row.Count > 5 then row.[5].ToString() else "";
          G = if row.Count > 6 then row.[6].ToString() else "";
          H = if row.Count > 7 then row.[7].ToString() else "";
          I = if row.Count > 8 then row.[8].ToString() else "";
          J = if row.Count > 9 then row.[9].ToString() else "";
          K = if row.Count > 10 then row.[10].ToString() else "";
          L = if row.Count > 11 then row.[11].ToString() else "";
          M = if row.Count > 12 then row.[12].ToString() else "";
          N = if row.Count > 13 then row.[13].ToString() else "";
          O = if row.Count > 14 then row.[14].ToString() else "";
          P = if row.Count > 15 then row.[15].ToString() else ""; }
    
    let matrix = ResizeArray<ResizeArray<int>>()
    let dataGridMatrix = ObservableCollection<DataGridRow>()

    let generateMatrix() = 
        matrix.Clear()
        for i in 1..nodeCount do
            let row = ResizeArray<int>()
            for j in 1..nodeCount do
                if i <> j then
                    row.Add(random.Next(minWeight, maxWeight))
                else
                    row.Add(0)
            matrix.Add(row)
        dataGridMatrix.Clear()
        for i in 0..matrix.Count - 1 do
            dataGridMatrix.Add(rowToRecord nodeNames.[i] matrix.[i])

    let populationToLog msg = 
        log.AppendLine(msg) |> ignore
        for chromosome in geneticAlgorithm.Value.Population do
            log.AppendLine(chromosome.ToString() + sprintf " (%d)" ((pathManager.Value :> IGeneticAlgorithmDelegate).Fitness(chromosome))) |> ignore
        this.OnPropertyChanged "Log"

    let updateMatrix() =
        try
            for i in 0..matrix.Count - 1 do
                let properties = dataGridMatrix.[i].GetType().GetProperties()
                for j in 0..matrix.[i].Count - 1 do
                    let matrixValue = matrix.[i].[j].ToString()
                    let dataGridValue = (properties.[j + 1].GetValue(dataGridMatrix.[i], null) :?> string)
                    if matrixValue <> dataGridValue then
                        matrix.[i].[j] <- int(dataGridValue)
            true
        with
        | _ -> MessageBox.Show("Only integer numbers are allowed in the matrix") |> ignore; false

    let initializeAlgorithm() = 
        if startNodeIndex = -1 then
            MessageBox.Show("You should select start node") |> ignore
        elif endNodeIndex = -1 then
            MessageBox.Show("You should select end node") |> ignore
        elif startNodeIndex = endNodeIndex then
            MessageBox.Show("Start and end nodes should be different") |> ignore
        elif nodeCount < chromosomeLength then
            MessageBox.Show("Node count can't be less then chromosome length") |> ignore
        elif nodeCount <> matrix.Count then
            MessageBox.Show("You should regenerate matrix") |> ignore 
        elif matrix.Count = 0 then
            MessageBox.Show("You should generate matrix before start") |> ignore
        elif updateMatrix() then
            isRunMode <- true
            this.OnPropertyChanged "IsRunMode"
            this.OnPropertyChanged "IsNotRunMode"
            pathManager <- Some(PathManager(
                                    nodeNames.[startNodeIndex],
                                    nodeNames.[endNodeIndex],
                                    nodeNames |> Seq.map (fun name -> (name, Array.IndexOf(nodeNames, name))) |> Map.ofSeq,
                                    matrix,
                                    chromosomeLength,
                                    crossPoints,
                                    generationSparseProbability,
                                    mutationSparseProbability))
            geneticAlgorithm <- Some(GeneticAlgorithm(pathManager.Value, populationSize))
            (geneticAlgorithm.Value :> IGeneticAlgorithm).GeneratePopulation()
            log.Clear() |> ignore
            populationToLog "* Initial population *"
            this.OnPropertyChanged "BestResult"

    let step() =
        (geneticAlgorithm.Value :> IGeneticAlgorithm).Step()
        stepCount <- stepCount + 1
        populationToLog (sprintf "\n* Step %d *" stepCount)
        this.OnPropertyChanged "BestResult"

    let runToComplete() =
        for i in 1..generationCount do
            (geneticAlgorithm.Value :> IGeneticAlgorithm).Step()
            iterationCount <- iterationCount + 1
            populationToLog (sprintf "\n* Iteration %d *" iterationCount)
        this.OnPropertyChanged "BestResult"

    let stop() = 
        isRunMode <- false
        this.OnPropertyChanged "IsRunMode"
        this.OnPropertyChanged "IsNotRunMode"
        
    member this.ExitCommand =
        RelayCommand ((fun _ -> true), (fun _ -> Application.Current.Shutdown()))

    member this.NextStepCommand =
        RelayCommand ((fun _ -> true), (fun _ -> step()))

    member this.RunToCompleteCommand =
        RelayCommand ((fun _ -> true), (fun _ -> runToComplete()))

    member this.GenerateMatrixCommand =
        RelayCommand ((fun _ -> true), (fun _ -> generateMatrix()))

    member this.AboutCommand = 
        RelayCommand ((fun _ -> true), (fun _ -> MessageBox.Show("Artem Ustimov (c) 2017") |> ignore))

    member this.ApplyCommand =
        RelayCommand ((fun _ -> true), (fun _ -> initializeAlgorithm()))

    member this.StopCommand = 
        RelayCommand ((fun _ -> true), (fun _ -> stop()))

    member this.ScrollCommand = 
        RelayCommand ((fun _ -> true), (fun sender -> (sender :?> TextBox).ScrollToEnd() |> ignore))

    member this.Matrix
        with get() = dataGridMatrix

    member this.NodeCount
        with get() = nodeCount
        and set value =
            nodeCount <- value
            this.OnPropertyChanged "NodeCount"
            this.OnPropertyChanged "Nodes"

    member this.Nodes = nodeNames |> Seq.take nodeCount

    member this.ChromosomeLength
        with get() = chromosomeLength
        and set value =
            chromosomeLength <- value
            this.OnPropertyChanged "ChromosomeLength"
            this.OnPropertyChanged "PopulationSizeMaximum"
            this.OnPropertyChanged "MaximumCrossPoints"                                

    // Factorial is max number of node permutations
    member this.PopulationSizeMaximum
        with get() = factor (chromosomeLength - 2) + 1

    member this.PopulationSize
        with get() = populationSize
        and set value = populationSize <- value

    member this.GenerationSparseProbability
        with get() = generationSparseProbability
        and set value = generationSparseProbability <- value

    member this.MaximumCrossPoints
        with get() = chromosomeLength - 1

    member this.CrossPoints
        with get() = crossPoints
        and set value =
            crossPoints <- value
            this.OnPropertyChanged "CrossPoints"

    member this.MutationSparseProbability
        with get() = mutationSparseProbability
        and set value = mutationSparseProbability <- value

    member this.IsRunMode
        with get() = isRunMode

    member this.IsNotRunMode
        with get() = not isRunMode

    member this.StartNode
        with get() = startNodeIndex
        and set value = startNodeIndex <- value

    member this.EndNode
        with get() = endNodeIndex
        and set value = endNodeIndex <- value

    member this.Log
        with get() = log.ToString()
        and set (value:string) = value |> ignore

    member this.GenerationCount
        with get() = generationCount
        and set value = generationCount <- value

    member this.BestResult
        with get() = match geneticAlgorithm with
                     | None -> "None"
                     | Some(ga) -> match ga.BestChromosome with
                                   | None -> "None"
                                   | Some(chromosome) -> chromosome.ToString() + sprintf " (%d)" ((pathManager.Value :> IGeneticAlgorithmDelegate).Fitness(chromosome))
                                   