﻿module MainApp

open System
open System.Windows

type MainWindow = FsXaml.XAML<"MainWindow.xaml">

[<STAThread>]
[<EntryPoint>]
let main _ = 
    let app = Application()
    let mainWindow = MainWindow()
    app.Run(mainWindow)