﻿namespace App.Model

type DataGridRow =
    { Node: string;
      mutable A: string;
      mutable B: string;
      mutable C: string;
      mutable D: string;
      mutable E: string;
      mutable F: string;
      mutable G: string;
      mutable H: string;
      mutable I: string;
      mutable J: string;
      mutable K: string;
      mutable L: string;
      mutable M: string;
      mutable N: string;
      mutable O: string;
      mutable P: string; }