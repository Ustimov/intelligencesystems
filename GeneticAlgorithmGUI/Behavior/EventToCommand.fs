﻿namespace App.Behavior

open System
open System.Windows
open System.Windows.Input
open System.Windows.Interactivity

type EventToCommand() =
    inherit TriggerAction<DependencyObject>()
    [<DefaultValue(false)>] static val mutable private CommandProperty:DependencyProperty
    [<DefaultValue(false)>] static val mutable private CommandParameterProperty:DependencyProperty
    
    static do 
        EventToCommand.CommandProperty <-
            DependencyProperty.Register("Command", typeof<ICommand>, typeof<EventToCommand>)
    
    static do 
        EventToCommand.CommandParameterProperty <-
            DependencyProperty.Register("CommandParameter", typeof<obj>, typeof<EventToCommand>)
    
    member this.Command 
        with get() = this.GetValue EventToCommand.CommandProperty :?> ICommand
        and set (value: ICommand) = this.SetValue(EventToCommand.CommandProperty, value)
    
    member this.CommandParameter 
        with get() = this.GetValue EventToCommand.CommandParameterProperty 
        and set value = this.SetValue(EventToCommand.CommandParameterProperty, value)
    
    override this.Invoke parameter = 
        let command = this.Command
        let commandParameter = match this.CommandParameter with
                               | null -> parameter
                               | commandParam -> commandParam  
        if command <> null && command.CanExecute(commandParameter) then
            command.Execute(commandParameter)