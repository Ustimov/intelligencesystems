﻿namespace GeneticAlgorithm

open System
open System.Text

module PathUtils = 
    // Fisher-Yates shuffle
    // http://stackoverflow.com/questions/273313/randomize-a-listt
    let shufflePath(random: Random, path: ResizeArray<string>) = 
        let mutable n = path.Count
        while n > 1 do
            n <- n - 1
            let k = random.Next(n + 1)
            let value = path.[k]
            path.[k] <- path.[n]
            path.[n] <- value

    let generatePath(random: Random, startNode: string, endNode: string, otherNodes: ResizeArray<string>, generateSparseProbability: float) = 
        let mutable leftNodes = ResizeArray<string>(otherNodes)
        shufflePath(random, leftNodes)
        if generateSparseProbability > 0.0 then
            leftNodes <- leftNodes
                |> Seq.map (fun node -> if random.NextDouble() <= generateSparseProbability then "0" else node) 
                |> (fun sparsedNodes -> ResizeArray<string>(sparsedNodes))
        leftNodes.Insert(0, startNode)
        leftNodes.Add(endNode)
        leftNodes

    let changeRandomNode(random: Random, path: ResizeArray<string>, otherNodes: ResizeArray<string>, changeSparseProbability: float) =
        let newNodeIndex = random.Next(otherNodes.Count)
        let oldNodeIndex = random.Next(1, path.Count - 1)
        if random.NextDouble() <= changeSparseProbability then
            path.[oldNodeIndex] <- "0"
        elif path.Contains(otherNodes.[newNodeIndex]) then
            // If a new node already exits when swap it with other node
            let duplicateNodeIndex = path.FindIndex(fun node -> node = otherNodes.[newNodeIndex])
            path.[duplicateNodeIndex] <- path.[oldNodeIndex]
            path.[oldNodeIndex] <- otherNodes.[newNodeIndex]
        else
            path.[oldNodeIndex] <- otherNodes.[newNodeIndex]

    let pathLength(path: ResizeArray<string>, nodeMap: Map<string, int>, nodeMatrix: ResizeArray<ResizeArray<int>>) = 
        let mutable sum = 0
        let mutable previousNodeMatrixIndex = nodeMap.[path.[0]];
        for i in 1..path.Count - 1 do
            if path.[i] <> "0" then
                let currentNodeMatrixIndex = nodeMap.[path.[i]]
                sum <- sum + nodeMatrix.[previousNodeMatrixIndex].[currentNodeMatrixIndex]
                previousNodeMatrixIndex <- currentNodeMatrixIndex
        sum

    let crossPaths(firstPath: ResizeArray<string>, secondPath: ResizeArray<string>, crossPointCount: int) =
        let step = firstPath.Count / (crossPointCount + 1)
        let mutable newPath = firstPath.GetRange(0, step)
        let mutable donorPath = secondPath
        for i in 1..crossPointCount - 1 do
            newPath.AddRange(donorPath.GetRange(i * step, step))
            donorPath <- match donorPath with
                            | _ when donorPath = firstPath -> secondPath
                            | _ -> firstPath
        newPath.AddRange(donorPath.GetRange(crossPointCount * step, firstPath.Count - crossPointCount * step))
        newPath

type IChromosome = interface end

type Path(nodes: ResizeArray<string>) =
    member this.Nodes = nodes
    override this.ToString() = 
        nodes |> Seq.fold (+) String.Empty
    override this.GetHashCode() = 
        this.ToString().GetHashCode()
    override this.Equals(obj) =
        this.ToString().Equals(obj.ToString())
    interface IChromosome

type IGeneticAlgorithmDelegate =
    abstract member NewChromosome: unit -> IChromosome
    abstract member CrossingOver: IChromosome * IChromosome -> IChromosome
    abstract member Mutate: IChromosome -> unit
    abstract member Fitness: IChromosome -> int

type PathManager(startNode: string, 
                 endNode: string,
                 nodeMap: Map<string, int>,
                 nodeMatrix: ResizeArray<ResizeArray<int>>,
                 pathNodeCount: int,
                 crossPointCount: int,
                 generateSparseProbability: float,
                 changeSparseProbability: float) = 
    
    let random = Random()

    let otherNodes = nodeMap 
                     |> Seq.choose (fun m -> if m.Key <> startNode && m.Key <> endNode then Some(m.Key) else None)
                     |> Seq.take (pathNodeCount - 2)
                     |> (fun nodes -> ResizeArray<string>(nodes))

    interface IGeneticAlgorithmDelegate with
        member this.NewChromosome() =
            Path(PathUtils.generatePath(random, startNode, endNode, otherNodes, generateSparseProbability))
            :> IChromosome

        member this.CrossingOver(firstPath, secondPath) =
            Path(PathUtils.crossPaths((firstPath :?> Path).Nodes, (secondPath :?> Path).Nodes, crossPointCount))
            :> IChromosome

        member this.Mutate(path) = 
            PathUtils.changeRandomNode(random, (path :?> Path).Nodes, otherNodes, changeSparseProbability)

        member this.Fitness(path) =
            PathUtils.pathLength((path :?> Path).Nodes, nodeMap, nodeMatrix)

type IGeneticAlgorithm =
    abstract member GeneratePopulation: unit -> unit
    abstract member CrossingOver: unit -> unit
    abstract member Mutation: unit -> unit
    abstract member Selection: unit -> unit
    abstract member Step: unit -> unit

type GeneticAlgorithm(geneticAlgorithmDelegate: IGeneticAlgorithmDelegate, populationSize: int) = 
    let mutable population = ResizeArray<IChromosome>()
    let mutable bestChromosome: Option<IChromosome> = None

    member this.Population = population
    member this.BestChromosome = bestChromosome
       
    interface IGeneticAlgorithm with
        member this.GeneratePopulation() = 
            while population.Count < populationSize do
                let newChromosome = geneticAlgorithmDelegate.NewChromosome()
                if not (population.Contains(newChromosome)) then
                    population.Add(newChromosome)
            (this :> IGeneticAlgorithm).Selection()

        member this.CrossingOver() = 
            let newPopulation = ResizeArray<IChromosome>()
            for i in 0..population.Count - 1 do
                for j in i..population.Count - 1 do
                    let newChromosome = geneticAlgorithmDelegate.CrossingOver(population.[i], population.[j])
                    newPopulation.Add(newChromosome)
            population <- newPopulation

        member this.Mutation() =
            for i in 0..population.Count - 1 do
                geneticAlgorithmDelegate.Mutate(population.[i])

        member this.Selection() = 
            let selection = population 
                            |> Seq.sortBy (fun chromosome -> geneticAlgorithmDelegate.Fitness(chromosome))
                            |> Seq.take populationSize
                            |> (fun seq -> ResizeArray<IChromosome>(seq))
            population <- selection
            match bestChromosome with
            | Some(chromosome) when geneticAlgorithmDelegate.Fitness(population.[0]) < geneticAlgorithmDelegate.Fitness(chromosome) -> bestChromosome <- Some(population.[0])
            | Some(chromosome) -> ()
            | None -> bestChromosome <- Some(population.[0])

        member this.Step() =
            (this :> IGeneticAlgorithm).CrossingOver()
            (this :> IGeneticAlgorithm).Mutation()
            (this :> IGeneticAlgorithm).Selection()
