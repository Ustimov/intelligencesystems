﻿namespace GeneticAlgorithmTests

open System
open NUnit.Framework
open GeneticAlgorithm.PathUtils

[<TestFixture>]
type PathUtilsTests() =

    let random = Random()

    let distinctedStringPath(path: ResizeArray<string>) = 
        path |> Seq.distinct |> Seq.fold (+) String.Empty
    
    [<Test>]
    member this.ShufflePathTests() = 
        let path = ResizeArray<string>(["A"; "B"; "C"; "D"; "E"])
        let pathAsString = path |> Seq.fold (+) String.Empty

        let mutable shuffledPath = ResizeArray<string>(path)
        shufflePath(random, shuffledPath)
        
        Assert.AreEqual(path.Count, shuffledPath.Count, "Different node count in path after shuffle")

        shuffledPath <- shuffledPath |> Seq.distinct |> (fun nodes -> ResizeArray<string>(nodes))
        let shuffledPathAsString = shuffledPath |> Seq.fold (+) String.Empty

        Assert.AreEqual(pathAsString.Length, shuffledPathAsString.Length, "Initial or shuffled path contains duplicate nodes")
        Assert.AreNotEqual(pathAsString, shuffledPathAsString, "Path is not changed after shuffle")

    [<Test>]
    member this.GeneratePathTests() = 
        let startNode = "A";
        let endNode = "B"
        let otherNodes = ResizeArray<string>(["C"; "D"; "E"])

        let noSparsePath = generatePath(random, startNode, endNode, otherNodes, 0.0)
                                |> distinctedStringPath

        Assert.AreEqual(5, noSparsePath.Length, "No sparse path contains duplicate nodes")
        Assert.AreEqual('A', noSparsePath.[0], "Wrong start node in no sparse path")
        Assert.AreEqual('B', noSparsePath.[4], "Wrong end node in no sparse path")

        let fullSparsePath = generatePath(random, startNode, endNode, otherNodes, 1.0)
                             |> distinctedStringPath

        Assert.AreEqual("A0B", fullSparsePath, "Wrong full sparse path")

    [<Test>]
    member this.ChangeRandomNodeTests() =
        let path = ResizeArray<string>(["A"; "B"; "C"; "D"; "E"])
        let otherNodes = path.GetRange(1, 3)

        changeRandomNode(random, path, otherNodes, 0.0)   
        let changedPathNoSparse = path |> distinctedStringPath

        Assert.AreEqual(path.Count, changedPathNoSparse.Length, "After changing no sparse path contains duplicate elements")
        Assert.AreEqual('A', changedPathNoSparse.[0], "Wrong start node in no sparse path")
        Assert.AreEqual('E', changedPathNoSparse.[4], "Wrong end node in no sparse path")


        changeRandomNode(random, path, otherNodes, 1.0)
        let changedPathSparse = path |> distinctedStringPath

        Assert.AreEqual(path.Count, changedPathSparse.Length, "After changing sparse path contains duplicate elements")
        Assert.AreEqual('A', changedPathSparse.[0], "Wrong start node in sparse path")
        Assert.AreEqual('E', changedPathSparse.[4], "Wrong end node in sparse path")
        Assert.IsTrue(path.Contains("0"), "Sparse path doesn't contain 0")

    [<Test>]
    member this.PathLengthTests() = 
        let nodeMatrix = ResizeArray<ResizeArray<int>>()

        [[1; 2; 3];
        [4; 5; 6];
        [7; 8; 9];] 
        |> Seq.map (fun row -> ResizeArray<int>(row)) 
        |> Seq.iter nodeMatrix.Add

        let nodeMap = Map<string, int>(["A", 0; "B", 1; "C", 2])

        Assert.AreEqual(8, pathLength(ResizeArray<string>(["A"; "B"; "C"]), nodeMap, nodeMatrix), "Wrong length for ABC path")
        Assert.AreEqual(9, pathLength(ResizeArray<string>(["C"; "C"]), nodeMap, nodeMatrix), "Wrong length for CC path")
        Assert.AreEqual(13, pathLength(ResizeArray<string>(["B"; "C"; "A"]), nodeMap, nodeMatrix), "Wrong length for BCA path")
        Assert.AreEqual(3, pathLength(ResizeArray<string>(["A"; "0"; "C"]), nodeMap, nodeMatrix), "Wrong length for A0C path")

    [<Test>]
    member this.CrossPathsTests() = 
        let firstPath = ResizeArray<string>(["A"; "B"; "C"; "D"])
        let secondPath = ResizeArray<string>(["E"; "F"; "G"; "H"])

        let onePointCrossedPath = crossPaths(firstPath, secondPath, 1) |> distinctedStringPath
        Assert.AreEqual("ABGH", onePointCrossedPath, "Wrong paths crossing with one point")

        let twoPointCrossedPath = crossPaths(firstPath, secondPath, 2) |> distinctedStringPath
        Assert.AreEqual("AFCD", twoPointCrossedPath, "Wrong paths crossing with two points")

        let threePointCrossedPath = crossPaths(secondPath, firstPath, 3) |> distinctedStringPath
        Assert.AreEqual("EBGD", threePointCrossedPath, "Wrong paths crossing with three points")
