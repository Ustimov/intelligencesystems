﻿namespace GeneticAlgorithmTests

open NUnit.Framework
open GeneticAlgorithm

[<TestFixture>]
type PathTests() = 
    [<Test>]
    member this.PathToStringTest() = 
        let nodes = ResizeArray<string>(["A"; "B"; "C"; "D"; "E"])
        let path = Path(nodes)
 
        Assert.AreEqual("ABCDE", path.ToString(), "Incorrect path's to string transform")

    [<Test>]
    member this.ContainsWithPathTest() =
        let firstPath = Path(ResizeArray<string>([|"A"; "B"; "C";|]))
        let secondPath = Path(ResizeArray<string>([|"A"; "B"; "C";|]))

        let paths = ResizeArray<Path>()
        paths.Add(firstPath)
        if not (paths.Contains(secondPath)) then
            paths.Add(secondPath)

        Assert.AreEqual(1, paths.Count, "Contains for path doesn't work")

        let chromosomes = ResizeArray<IChromosome>()
        chromosomes.Add(firstPath)
        if not (chromosomes.Contains(secondPath)) then
            chromosomes.Add(secondPath)

        Assert.AreEqual(1, chromosomes.Count, "Contains for chromosome doesn't work")


